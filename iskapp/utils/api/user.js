import {
    getWithToken,
    post,
    postWithToken,
    uploadWithToken,
    showLoading,
} from "./http";

/**
 * 用户登录
 */
export function login(params) {
    return post("/user/login", params);
}
/**
 * 获取用户上课时间
 */
export function getClassTime() {
    return getWithToken("/usercourse/starttime");
}
/**
 * 查询当前用户基本信息
 */
export function info() {
    return getWithToken("/user/info");
}
/**
 * 更新用户基本信息
 */
export function userEdit(params) {
    return showLoading(postWithToken("/user/edit", params), "更新中");
}

/**
 * 查询情侣信息
 */
export function couplesInfo() {
    return getWithToken("/lovers/info");
}
/**
 * 获取情侣间留言列表
 */
export function couplesMsgList(params) {
    return getWithToken("/loversinfo/list", params);
}
/**
 * 绑定情侣信息
 */
export function couplesAdd(params) {
    return postWithToken("/lovers/add", params);
}
/**
 * 解绑情侣信息
 */
export function couplesDel() {
    return getWithToken("/lovers/del");
}
/**
 * 情侣背景上传
 * @param {*} filePath 图片url
 * @param {*} image 图片
 */
export function couplesBG(filePath, holder = {}) {
    return uploadWithToken({
        path: "/loversinfo/upimg",
        name: "image",
        filePath: filePath,
        formData: {},
        holder,
    });
}
/**
 * 情侣背景设置/留言修改
 * @param {*}
 * @return {*}
 */
export function couplesInfoAdd(params) {
    return postWithToken("/loversinfo/add", params);
}
/**
 * 情侣留言删除
 * @param {*}
 * @return {*}
 */
export function couplesInfoDel(params) {
    return postWithToken("/loversinfo/del", params);
}
/**
 * 设置课表背景
 * @param {*} filePath 图片url
 * @param {*} sorts 周课表为table，日课表为index
 * @param {*} image 图片
 */
export function scheduleBG(filePath, sorts, holder = {}) {
    return showLoading(
        uploadWithToken({
            path: "/usersetting/set",
            name: "image",
            filePath: filePath,
            formData: {
                sorts,
            },
            holder,
        }),
        "更新中"
    );
}

/**
 * 恢复默认背景
 * @param {*}
 * @return {*}
 */
export function setbgdefault() {
    return postWithToken("/usersetting/setbgdefault");
}
