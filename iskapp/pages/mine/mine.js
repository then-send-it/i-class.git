/* eslint-disable no-undef */
// +----------------------------------------------------------------------
// | 本插件基于GPL-3.0开源协议
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 https://www.lianshoulab.com/ All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( https://www.gnu.org/licenses/gpl-3.0.html )
// +----------------------------------------------------------------------
// | Author: liutu <17053613@qq.com>
// +----------------------------------------------------------------------
//index.js
//获取应用实例
const app = getApp();
import config from "../../config";
import { wxShowMaskLoading } from "../../utils/promisify";
import { serializePathQuery } from "../../utils/api/http";
Page({
    data: {
        StatusBar: app.globalData.StatusBar,
        CustomBar: app.globalData.CustomBar,
        ImgUrl: app.globalData.ImgUrl,
        displayArea: app.globalData.displayArea,
        gradeList: config.gradeList,
        settingList: [
            {
                name: "情侣课表",
                icon: "/images/couplesSchedule.png",
                url: "/pages/couples/couples?",
            },
            {
                name: "设置课表背景",
                icon: "/images/scheduleIcon.png",
                url: "/pages/schedule/schedule?",
            },
            {
                name: "设置",
                icon: "/images/setting.png",
                url: "/pages/setting/setting?",
            },
        ],
        userInfo: app.globalData.userInfo,
        hasUserInfo: app.globalData.userInfo,
    },
    onLoad: function () {
        app.userInfoReadyCallback = (res) => {
            this.setData({
                userInfo: res,
                hasUserInfo: true,
            });
        };
    },
    onShow() {
        if (app.globalData.userInfo) {
            this.setData({
                userInfo: app.globalData.userInfo,
                hasUserInfo: true,
            });
        }
    },
    bindGetUserInfo(e) {
        if (e.detail.userInfo) {
            wxShowMaskLoading("登陆中");
            app.getInfo().then((v) => {
                console.log(v, "getInfo");
                wx.hideLoading();
                this.setData({
                    userInfo: v,
                    hasUserInfo: true,
                });
            });
        }
    },
    /**
     * 跳转到新页面设置
     * @param {*} url 传入的跳转地址
     */
    navigationTo(e) {
        let url = e.target.dataset.url;
        let { lovers_id } = this.data.userInfo;
        console.log(url);
        if (url == "/pages/couples/couples?") {
            wx.navigateTo({
                url:
                    url +
                    serializePathQuery({
                        couplesId: lovers_id ? lovers_id : null,
                    }),
            });
        } else {
            wx.navigateTo({
                url,
            });
        }
    },
});
