/* eslint-disable no-undef */
// +----------------------------------------------------------------------
// | 本插件基于GPL-3.0开源协议
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 https://www.lianshoulab.com/ All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( https://www.gnu.org/licenses/gpl-3.0.html )
// +----------------------------------------------------------------------
// | Author: liutu <17053613@qq.com>
// +----------------------------------------------------------------------
//index.js
//获取应用实例
const app = getApp();
import { couplesInfo, couplesDel, couplesMsgList } from "../../utils/api/user";
import { serializePathQuery } from "../../utils/api/http";
import dayjs from "../../utils/dayjs/dayjs.min";
import { wxShowToast } from "../../utils/promisify";

Page({
    data: {
        StatusBar: app.globalData.StatusBar,
        CustomBar: app.globalData.CustomBar,
        displayArea: app.globalData.displayArea,
        ImgUrl: app.globalData.ImgUrl,
        couplesId: "",
        removeBind: false,
        userInfo: app.globalData.userInfo,
        messageList: null,
    },
    onLoad: function (query) {
        console.log(query, this.data.displayArea, "query");
        let couplesId =
            query.couplesId && query.couplesId !== "null"
                ? query.couplesId
                : null;
        if (couplesId) {
            this.setData({
                userInfo: app.globalData.userInfo,
                couplesId,
            });
            couplesMsgList({
                is_show: 1,
                tid: couplesId,
            }).then((v) => {
                console.log(v.data);
                let messageList = v.data.data.length > 0 ? v.data.data : [];
                messageList = messageList.filter(
                    (v) => v.love_sort_text !== "Love_sort 1"
                );
                messageList = messageList.map((v) => {
                    v.time = dayjs
                        .unix(v.starttime)
                        .format("YYYY年MM月DD日 HH:mm");
                    return v;
                });
                this.setData({
                    messageList,
                });
            });
            couplesInfo().then((v) => {
                this.setData({
                    loverInfo: v.data,
                });
            });
        }
    },
    /**
     * 后退一页
     */
    BackPage() {
        wx.navigateBack({
            delta: 1,
        });
    },
    /**
     * 取消绑定弹窗
     */
    removeBind() {
        this.setData({
            removeBind: true,
        });
    },
    /**
     * 取消绑定确认按钮
     */
    removeBindConfirm(e) {
        let type = e.currentTarget.dataset.type;
        if (type) {
            couplesDel().then((v) => {
                v.code && wxShowToast("解绑成功");
                app.getSet();
            });
        }
        this.setData({
            removeBind: false,
        });
        app.getSet().then(() => {
            this.BackPage();
        });
    },
    /**
     * 分享邀请
     */
    onShareAppMessage: function (options) {
        console.log(options);
        let nickname = app.globalData.userInfo.nickname;
        let avatar = app.globalData.userInfo.avatar;
        let id = app.globalData.userInfo.user_id;
        if (options.from == "button") {
            let shareObj = {
                title: `${nickname}请求和你绑定成情侣`,
                path:
                    "/pages/index/index?" +
                    serializePathQuery({
                        couplesAdd: id,
                        nickname,
                        avatar,
                    }),
                imageUrl: "/images/share.png", //自定义图片路径，可以是本地文件路径、代码包文件路径或者网络图片路径，支持PNG及JPG，
                success: (res) => {
                    // 转发成功之后的回调
                    if (res.errMsg == "shareAppMessage:ok") {
                        return;
                    }
                },
                fail: () => {
                    // 转发失败之后的回调
                    if (res.errMsg == "shareAppMessage:fail cancel") {
                        return;
                    } else if (res.errMsg == "shareAppMessage:fail") {
                        return;
                    }
                },
            };
            return shareObj;
        }
    },
});
